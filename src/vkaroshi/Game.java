/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vkaroshi;

/**
 *
 * @author vk
 */


public class Game {
    protected GameScore score;
    private GameType type;
    private GameStateEnum state = GameStateEnum.NOT_STARTED;
    protected int currentRow = 0;
    private int[] bombs;
    
    public void initScore(javax.swing.JLabel display){
        this.score = new GameScoreVariable(display);
    }
    
    public void startGame(GameType t){
        if(score.getScore()<0.01) {
            throw new IllegalArgumentException("You don't have enough money to play.");
        }
        else if(t.getBet() > score.getScore()) {
            throw new IllegalArgumentException("Your bet cannot be higher than your balance.");
        }
        this.type = t;
        this.setBombs();
        this.state = GameStateEnum.RUNNING;
        this.currentRow = 1;
        this.score.adjustScore(-this.type.getBet());
    }
    
    public boolean playRow(int i){
        if(this.bombs[currentRow]==i){
            this.state = GameStateEnum.LOST;
            return false;
        } {
            return true;
        }
    }
    
    public void nextRound() {
            this.currentRow++;    
    }
    
    public double getCurrentWin(){
        if(this.currentRow<2) { return this.type.getBet(); }
        else { return this.type.getBet()*this.type.getRates(currentRow-1);}
    }
    
    
    public double endGame(){
        if(this.state==GameStateEnum.RUNNING) {
            this.state = GameStateEnum.WIN;
            double win = this.getCurrentWin();
            this.score.adjustScore(win);
            return win;
        }
        return 0;
    }
    
    protected void setBombs(){
        int max = this.type.getCols();
        this.bombs = new int[this.type.getRows()+1];
        for(int i =1; i <= this.bombs.length-1; i++){
            this.bombs[i] = 1 + (int)(Math.random()*max); 
        }
    }
    
    protected int[] getBombs(){
        return this.bombs;
    }
    
    protected int getBombs(int i){
        return this.bombs[i];
    }
    
    public GameStateEnum getState() {
        return state;
    }

    public void setState(GameStateEnum state) {
        this.state = state;
    }
    
    public boolean isRunning(){
        if(this.state==GameStateEnum.RUNNING) { return true; }
        return false;
    }
    
    public boolean isWin(){
        if(this.state==GameStateEnum.WIN) { return true; }
        return false;
    }    
}
