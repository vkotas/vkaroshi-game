/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vkaroshi;

/**
 *
 * @author vk
 */
public class GameType {
    private int cols = 0;
    private int rows = 0;
    private double bet = 0;
    private double[] rates;

    public GameType(int cols, int rows, double bet) {
        this.cols = cols;
        this.rows = rows;
        this.bet = bet;
    }
    
    public GameType(){}
    
    public void setSize(int cols, int rows) {
        this.cols = cols;
        this.rows = rows;        
        this.calcRates(cols, rows);    
    }
    
    private void calcRates(int cols, int rows){
        this.rates = new double[rows+1];
        for(int i=1; i <= rows; i++){
            this.rates[i] = Math.pow((1+(1/(double)cols)), i);
            
        }
    }
    
    public int getCols() {
        return cols;
    }
    
    public double[] getRates(){
        return rates;
    }
    
    public double getRates(int i) {
        return rates[i];
    }


    public int getRows() {
        return rows;
    }

    public double getBet() {
        return bet;
    }

    public void setBet(double bet) {
        this.bet = bet;
    }
}
