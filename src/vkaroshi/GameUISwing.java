/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vkaroshi;

import java.awt.Color;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.AbstractButton;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

/**
 *
 * @author vk
 */
public class GameUISwing extends Game {
    private javax.swing.JTextArea jTextAreaLog;
    private GameType type;
    private javax.swing.JPanel jPanelGame;
    private javax.swing.JButton[][] jButtonGame;
    private javax.swing.ButtonGroup buttonGroupBets;
    private javax.swing.JLabel balance;
    private javax.swing.JComboBox jComboBoxMapSize;
    private javax.swing.JButton jButtonStart;
    private Icon icon_ok;
    private Icon icon_bomb;
    private Icon icon_fail;
    private Icon icon_coins;
    private GameJFrame frame;
    
    public GameUISwing(javax.swing.JTextArea jTextAreaLog, javax.swing.JPanel jPanelGame, javax.swing.ButtonGroup buttonGroupBets, javax.swing.JLabel balance, javax.swing.JComboBox jComboBoxMapSize, javax.swing.JButton jButtonStart, GameJFrame frame) {
        this.jTextAreaLog = jTextAreaLog;
        this.jPanelGame = jPanelGame;
        this.jComboBoxMapSize = jComboBoxMapSize;
        this.buttonGroupBets = buttonGroupBets;
        this.jButtonStart = jButtonStart;
        this.icon_ok = new ImageIcon(getClass().getResource("/vkaroshi/resources/icons/step.png"));
        this.icon_bomb = new ImageIcon(getClass().getResource("/vkaroshi/resources/icons/bomb.png"));
        this.icon_fail = new ImageIcon(getClass().getResource("/vkaroshi/resources/icons/fail.gif"));            
        this.icon_coins = new ImageIcon(getClass().getResource("/vkaroshi/resources/icons/coins.png"));            
        this.frame = frame;
        
        this.initScore(balance);
        this.logAppend(String.format("Welcome to the VKaroshi game! You have received %s€ initial credit.", this.score.getScore()));        
    }
    
    public void mainButtonClick(){
        if(this.isRunning()) {
            this.takeWin();
        }
        else {
            this.startGameplay();
        }        
    }
    
    private void takeWin(){
            double result = this.endGame();
            this.showBombs();
            this.endGameplay(true);
            this.logAppend(String.format("You won %.2f€.", result));    
    }
    
    private void startGameplay() {
        try {
            this.toggleControls(false);
            this.startGame(this.type);
            this.resetGame();        
            this.logAppend(String.format("Game started. Your bet is %.2f€.", this.type.getBet()));                
            this.setTakelabel();
            this.enableRow();            
        }
        catch (IllegalArgumentException ex) {
            JOptionPane.showMessageDialog(this.frame, ex.toString());
            this.endGameplay(false);
        }    
    }
    
    private void endGameplay(boolean win){
            if(win) { this.jPanelGame.setBackground(Color.green); } else { this.jPanelGame.setBackground(Color.red);  } 
            this.disableGameButtons();
            this.setTakelabel(true);  
            this.toggleControls(true);            
    }
    
    private void enableRow(){
        for(int i=this.currentRow; i<=this.type.getRows(); i++){
            for(int j=1; j<=this.type.getCols(); j++) {
                if(i==this.currentRow) { jButtonGame[i][j].setEnabled(true); } else { jButtonGame[i][j].setEnabled(false); }
            }
        }
    }
    
    private void disableGameButtons(){
        for(int i=1; i<=this.type.getRows(); i++){
            for(int j=1; j<=this.type.getCols(); j++) {
                jButtonGame[i][j].setEnabled(false);
            }
        }
    }
    
    private void toggleControls(boolean b){
        Enumeration<AbstractButton> elements = buttonGroupBets.getElements();
        while (elements.hasMoreElements()) {
              AbstractButton button = (AbstractButton)elements.nextElement();
              button.setEnabled(b);
        }
        this.jComboBoxMapSize.setEnabled(b);
    }
    
    private void setTakelabel(boolean revert){
        String t = "";
        Icon i;
        if(revert) { t = "Start game"; i = this.icon_bomb;}
        else { t = String.format("Take %.2f€", this.getCurrentWin()); i = this.icon_coins; }
        jButtonStart.setText(t);
        jButtonStart.setIcon(i);
    }
    
    private void setTakelabel() {
        this.setTakelabel(false);
    }
    
    public void setType(int cols, int rows, double bet){
        this.type = new GameType();
        this.type.setBet(bet);
        this.setSize(cols, rows);
    }
    
    public void resetGame(){
        this.setSize(this.type.getCols(), this.type.getRows());
        this.setBombs();
        this.jPanelGame.setBackground(null); 
    }
    
    
    public void setSize(int cols, int rows) {
        jButtonGame = new javax.swing.JButton[rows+1][cols+1];
        jPanelGame.removeAll();
        jPanelGame.setLayout(new java.awt.GridLayout(rows, cols+1, 1, 1));
        this.type.setSize(cols, rows);
        
        for(int i=1; i <= rows; i++){
            for(int j=1; j<=cols; j++){
                this.jButtonGame[i][j] = new javax.swing.JButton();
                this.jButtonGame[i][j].setEnabled(false);
                jPanelGame.add(this.jButtonGame[i][j]);
                this.jButtonGame[i][j].addActionListener(new GameButtomActionListener(j, i, this));
            }
        jPanelGame.add(new javax.swing.JLabel(String.format("x %.2f", this.type.getRates(i))));
        }
        jPanelGame.revalidate();
        jPanelGame.repaint();    
    }
    
    protected void gameButtonClick(int j, int i) {
        if(!this.isRunning()){
            throw new IllegalArgumentException("Game is not started");
        }
        
        if(i==this.currentRow) {
            this.showBomb();
            this.jButtonGame[this.currentRow][j].setIcon(icon_ok);
            this.gameButtonClickGoodRow(j);
        }
    }
    
    private void gameButtonClickGoodRow(int j){
        if(this.playRow(j)){
            if(this.currentRow==this.type.getRows()) { this.nextRound(); this.mainButtonClick(); }
            else {
            this.nextRound();            
            this.enableRow();
            this.setTakelabel(); 
            }
        }
        else {
            this.logAppend(String.format("Bang! You lost %.2f€.", this.type.getBet()));
            this.jButtonGame[this.currentRow][j].setIcon(icon_fail);
            this.showBombs(this.currentRow);
            this.endGameplay(false);
        }        
    }
    
    private void showBomb(int row){
        this.jButtonGame[row][this.getBombs(row)].setIcon(icon_bomb);
    }
    
    private void showBomb(){
        this.showBomb(this.currentRow);
    }
    
    private void showBombs(int e){
        for(int i=1;i<=this.type.getRows(); i++) {
            if(i!=e) { this.showBomb(i); }
        }
    }
    
    private void showBombs(){
        this.showBombs(0);
    }
            
    public void setBet(double bet, javax.swing.JButton but) {
        Enumeration<AbstractButton> elements = buttonGroupBets.getElements();
        while (elements.hasMoreElements()) {
              AbstractButton button = (AbstractButton)elements.nextElement();
              button.setBackground(null);
        }
        but.setBackground(Color.red);
        this.type.setBet(bet);
    }
    
    private void logAppend(String t){
        Date d = new Date();
        DateFormat df = new SimpleDateFormat("MM.dd.yyyy HH:mm:ss");
        String logDate = df.format(d);
        jTextAreaLog.append(String.format("%s - %s\n", logDate, t));    
    }
    
}
