/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vkaroshi;

/**
 *
 * @author vk
 */
public class GameScoreVariable implements GameScore {
    private double score = 0;
    private javax.swing.JLabel display;
    
    public GameScoreVariable(javax.swing.JLabel display){
        this.score = 10;
        this.display = display; 
        this.setDisplay();     
    }

    public GameScoreVariable(double d, javax.swing.JLabel display){
        this.score = d;
        this.display = display; 
        this.setDisplay();       
    }
    
    @Override
    public double getScore(){
        return this.score;
    }
    
    @Override
    public void setScore(double s){
        this.score = s;
        this.setDisplay();        
    }
    
    @Override
    public boolean adjustScore(double ch){
        this.score += ch;
        this.setDisplay();
        if(this.score < 0) { return false; }
        else { return true; }
    }
    
    private void setDisplay(){
        this.display.setText(String.format("%.2f", this.score));        
    }
}
