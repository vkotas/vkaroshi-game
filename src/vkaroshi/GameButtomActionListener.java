/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vkaroshi;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author vk
 */
public class GameButtomActionListener implements ActionListener {
    private final int col;
    private final int row;
    private final GameUISwing gameui;
    
    public GameButtomActionListener(final int col, final int row, final GameUISwing gameui) {
        super();
        this.col = col;
        this.row = row;
        this.gameui = gameui;
    }

    public void actionPerformed(ActionEvent evt) {
        gameui.gameButtonClick(this.col, this.row);
    }    
    
}
