/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vkaroshi;

/**
 *
 * @author vk
 */
public interface GameScore {
    public double getScore();
    public void setScore(double s);
    public boolean adjustScore(double ch);    
}
